﻿using Nager.HetznerDns;
using Nager.HetznerDns.Models;
using Newtonsoft.Json;
using OnSive.Hetzner.RDNS.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace OnSive.Hetzner.RDNS
{
    class Program
    {
        const string settingsName = "appsettings.json";
        static Config config;
        static HetznerDnsClient client;
        static Timer timer;

        static async Task Main(string[] args)
        {
            if (!File.Exists(settingsName))
            {
                config = new Config().Seed();
                File.WriteAllText(settingsName, JsonConvert.SerializeObject(config));
                Console.WriteLine("Please edit the appsettings.json and insert your zone and record ids, or just insert the API key and restart the application!");
            }
            else
            {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(settingsName));
                client = new HetznerDnsClient("MZpYjm8olX6nXwlZHLujEbYExpcWPPQp");

                if (config.Zones.Count == 1 && config.Zones[0].ZoneId == "DEMO_ZONE")
                {
                    string selection;
                    int selected;
                    Console.WriteLine("Fetching your zones...");
                    var zones = await client.GetZonesAsync().ConfigureAwait(false);
                    Console.WriteLine("Select a zone:");
                    for (int i = 0; i < zones.Zones.Length; i++)
                    {
                        Console.WriteLine("[" + i.ToString().PadLeft(zones.Zones.Length.ToString().Length) + "] " + zones.Zones[i].Name.PadRight(15) + " | " + zones.Zones[i].Id);
                    }
                    do
                    {
                        Console.Write("> ");
                        selection = Console.ReadLine();
                    } while (!int.TryParse(selection, out selected));

                    var selectedZone = zones.Zones[selected];
                    var configZone = config.Zones.FirstOrDefault(x => x.ZoneId == selectedZone.Id);
                    if (configZone == null)
                    {
                        configZone = new ConfigZone()
                        {
                            ZoneId = selectedZone.Id,
                            RecordIds = new()
                        };
                        config.Zones.Add(configZone);
                    }
                    await RecordSelectionAsync(selectedZone, configZone).ConfigureAwait(false);

                    File.WriteAllText(settingsName, JsonConvert.SerializeObject(config));
                }
                else
                {
                    timer = new(300000)
                    {
                        AutoReset = true,
                        Enabled = true
                    };
                    timer.Elapsed += Timer_Elapsed;
                    timer.Start();

                    Console.WriteLine("Press enter to exit...");
                    Console.ReadLine();
                }
            }
        }

        private static async Task RecordSelectionAsync(Zone zone, ConfigZone configZone)
        {
            string selection;
            int selected;

            Console.WriteLine("Fetching zone records...");
            var allRecords = await client.GetRecordsAsync(zone.Id).ConfigureAwait(false);
            var records = allRecords.Records.Where(x => x.Type == DnsRecordType.A || x.Type == DnsRecordType.AAAA).ToArray();

            Console.WriteLine("Select the records:");
            for (int i = 0; i < records.Length; i++)
            {
                Console.WriteLine("[" + i.ToString().PadLeft(records.Length.ToString().Length) + "] " + records[i].Name.PadRight(15) + " | " + records[i].Value + " | " + records[i].Id);
            }
            Console.WriteLine("Select which records should automatically be updated: (enter a number and confirm with enter - to return enter C)");
            do
            {
                selection = Console.ReadLine();
                if (!int.TryParse(selection, out selected))
                {
                    if (string.Compare(selection, "c", true) == 0)
                    {
                        // GO BACK
                    }
                }
                if (selected < records.Length && selected > 0)
                {
                    if (!configZone.RecordIds.Contains(records[selected].Id))
                        configZone.RecordIds.Add(records[selected].Id);
                }
            } while (!int.TryParse(selection, out selected));
        }

        private static async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
        }
    }
}

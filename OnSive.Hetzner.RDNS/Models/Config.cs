﻿using Nager.HetznerDns.Models;
using System;
using System.Collections.Generic;

namespace OnSive.Hetzner.RDNS.Models
{
    public class Config
    {
        public string ApiKey { get; set; }
        public TimeSpan Intervall { get; set; }
        public ulong TTL { get; set; }
        public Dictionary<DnsRecordType, bool> ActiveRecordTypes { get; set; } 
        public List<string> IPv4Provider { get; set; }
        public List<string> IPv6Provider { get; set; }
        public List<ConfigZone> Zones { get; set; }

        public Config Seed()
        {
            ApiKey = null;
            Intervall = TimeSpan.FromMinutes(5);
            TTL = 86400;
            IPv4Provider = new()
            {
                "http://ipv4.icanhazip.com",
                "http://bot.whatismyipaddress.com",
                "http://ipinfo.io/ip",
                "https://api.ipify.org",
            };
            ActiveRecordTypes = new()
            {
                { DnsRecordType.A, true },
                { DnsRecordType.AAAA, false }
            };
            IPv6Provider = new()
            {
                "http://ipv6.icanhazip.com",
            };
            Zones = new()
            {
                new ConfigZone().Seed()
            };
            return this;
        }
    }

    public class ConfigZone
    {
        public string ZoneId { get; set; }
        public List<string> RecordIds { get; set; }

        public ConfigZone Seed()
        {
            ZoneId = "DEMO_ZONE";
            RecordIds = new()
            {
                "RECORD_1",
                "RECORD_2",
            };
            return this;
        }
    }
}

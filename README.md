Small application which automatically updates Hetzner DNS Console records periodically with the current public IP of a machine.

---

Docker Image `registry.gitlab.com/onsive.net/hetzner.dyndns/hetznerdyndns:latest`

---

`docker-compose.yml`
```
version: '3'
services:
  dyndns:
    image: registry.gitlab.com/onsive.net/hetzner.dyndns/hetznerdyndns:latest
    container_name: dyndns
    volumes:
      - ./conf/:/etc/hetzner/
```
---
`appsettings.json`
```
{
  "ApiKey": "MZpY...",
  "Intervall": "00:05:00",
  "TTL": 86400,
  "ActiveRecordTypes": {
    "A": true,
    "AAAA": false
  },
  "IPv4Provider": [
    "http://ipv4.icanhazip.com",
    "http://bot.whatismyipaddress.com",
    "http://ipinfo.io/ip",
    "https://api.ipify.org"
  ],
  "IPv6Provider": [
    "http://ipv6.icanhazip.com"
  ],
  "Zones": [
    {
      "ZoneId": "qpUS...",
      "RecordIds": [
        "1239..."
      ]
    },
    {
      "ZoneId": "6s72...",
      "RecordIds": [
        "63e6..."
      ]
    },
    {
      "ZoneId": "7D4U...",
      "RecordIds": [
        "8a7a..."
      ]
    }
  ]
}
```

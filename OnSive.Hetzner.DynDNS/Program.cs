﻿using Nager.HetznerDns;
using Nager.HetznerDns.Models;
using Newtonsoft.Json;
using OnSive.Hetzner.DynDNS.Models;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OnSive.Hetzner.DynDNS
{
    internal class Program
    {
        private const string ConfigDirectory = "/etc/hetzner/";
        private const string ConfigFileName = "appsettings.json";
        private const string ConfigFile = ConfigDirectory + ConfigFileName;

        private static Logger log;
        private static Config config;
        private static HetznerDnsClient client;

        private static readonly Dictionary<DnsRecordType, string> lastIps = new();
        private static readonly Dictionary<string, string> zoneNames = new();
        private static readonly List<Record> records = new();

        private static async Task Main(string[] args)
        {
            log = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            if (!File.Exists(ConfigFile))
            {
                Directory.CreateDirectory(ConfigDirectory);
                config = new Config().Seed();
                File.WriteAllText(ConfigFile, JsonConvert.SerializeObject(config));
            }
            else
                LoadConfig();

            if (string.IsNullOrWhiteSpace(config.ApiKey))
            {
                log.Error($"No API key specified!{Environment.NewLine}Edit the {ConfigFileName} and restart the container.{Environment.NewLine}{Environment.NewLine}Exiting...");
                Environment.Exit(-1);
            }

            log.Information("Record type activation state:");
            foreach (var activeRecordTypes in config.ActiveRecordTypes)
            {
                log.Information($"  {activeRecordTypes.Key,6}: {activeRecordTypes.Value}");
                if (activeRecordTypes.Value)
                    lastIps.Add(activeRecordTypes.Key, "");
            }

            client = new HetznerDnsClient(config.ApiKey);
            var zones = await client.GetZonesAsync().ConfigureAwait(false);
            foreach (var zone in config.Zones)
            {
                var hetznerZone = zones.Zones.FirstOrDefault(x => x.Id == zone.ZoneId);
                if (hetznerZone == null)
                {
                    log.Warning($"Skipping zone {zone.ZoneId}: Zone does not exist!");
                    continue;
                }
                zoneNames.Add(hetznerZone.Id, hetznerZone.Name);
                var zoneRecords = await client.GetRecordsAsync(zone.ZoneId).ConfigureAwait(false);
                var validZoneRecords = zoneRecords.Records.Where(x => zone.RecordIds.Contains(x.Id));
                records.AddRange(validZoneRecords);
            }

            await MainLoop().ConfigureAwait(false);
        }

        private static async Task MainLoop()
        {
            log.Information("Running...");
            do
            {
                log.Information("Checking public IP");
                using (var client = new WebClient())
                {
                    await UpdateRecords(config.IPv4Provider, "IPv4", client, DnsRecordType.A).ConfigureAwait(false);
                    await UpdateRecords(config.IPv6Provider, "IPv6", client, DnsRecordType.AAAA).ConfigureAwait(false);
                }

                await Task.Delay(config.Intervall).ConfigureAwait(false);
            } while (true);
        }

        private static string GetPublicIp(WebClient client, List<string> providers)
        {
            string ip = null;
            foreach (var provider in providers)
            {
                try
                {
                    ip = client.DownloadString(provider);
                    if (ip.EndsWith('\n'))
                        ip = ip[..^1];
                    break;
                }
                catch { }
            }
            return ip;
        }

        private static async Task UpdateRecords(List<string> provider, string ipName, WebClient client, DnsRecordType dnsRecordType)
        {
            if (!config.ActiveRecordTypes.ContainsKey(dnsRecordType) || !config.ActiveRecordTypes[dnsRecordType])
                return;

            var ip = GetPublicIp(client, provider);
            log.Information("  " + ipName + ": " + ip);
            await UpdateRecords(ip, dnsRecordType).ConfigureAwait(false);
        }

        private static async Task UpdateRecords(string ip, DnsRecordType dnsRecordType)
        {
            if (lastIps[dnsRecordType] == ip)
            {
                log.Information("Skipping " + dnsRecordType + " because of identical public IP (" + ip + ")");
                return;
            }

            if (ip == null)
            {
                log.Warning("Skipping " + dnsRecordType + " because of public IP could not be detected!");
                return;
            }

            log.Information("Update " + dnsRecordType + " records to " + ip);
            lastIps[dnsRecordType] = ip;
            foreach (var record in records.Where(x => x.Type == dnsRecordType))
            {
                var updateRecord = new UpdateRecord()
                {
                    Name = record.Name,
                    Ttl = config.TTL,
                    Type = record.Type,
                    ZoneId = record.ZoneId,
                    Value = ip
                };

                try
                {
                    await client.UpdateRecordAsync(record.Id, updateRecord).ConfigureAwait(false);
                    log.Information("  SUCCESS - " + record.Name + "." + zoneNames[record.ZoneId]);
                }
                catch (Exception ex)
                {
                    log.Error(ex, "  FAILED  - " + record.Name + "." + zoneNames[record.ZoneId]);
                }
            }
        }

        private static void LoadConfig()
        {
            var content = File.ReadAllText(ConfigFile);
            config = JsonConvert.DeserializeObject<Config>(content);
        }
    }
}
